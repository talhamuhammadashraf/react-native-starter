import React, {Fragment, Component} from 'react';
import {
  View,
  StatusBar,
  NativeModules,
  BackHandler,
  Platform,
  SafeAreaView,
} from 'react-native';
import configureStore, {AppWithNavigationState} from './src/store';
import {Provider} from 'react-redux';
import AppNavigator from './src/navigation';
import {NavigationActions} from 'react-navigation';
// import KeyboardManager from 'react-native-keyboard-manager';
class App extends Component {
  state = {
    isLoading: true,
    store: configureStore(() => {
      this.setState({isLoading: false});
      if (Utils.isPlatformAndroid()) {
        NativeModules.SplashScreen.hide();
      }
    }),
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    if (Platform.OS === 'ios') {
      // KeyboardManager.setToolbarPreviousNextButtonEnable(true);
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    KeyboardManager.setToolbarPreviousNextButtonEnable(false);
  }

  onBackPress = () => {
    const {dispatch, getState} = this.state.store;
    const {nav} = getState();
    if (nav.index === 0) {
      return false;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    if (this.state.isLoading) {
      return null;
    } else {
      return (
        <View style={{flex: 1}}>
          <SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <Provider store={this.state.store}>
              <AppWithNavigationState />
            </Provider>
          </SafeAreaView>
        </View>
      );
    }
  }
}

export default App;
