/**
 * @format
 */
import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';
import { name as appName } from './app.json';
import App from './App';

AppRegistry.registerComponent(appName, () => App);
