import {StyleSheet} from 'react-native';
import { Metrics, Colors } from '../../theme';


export default StyleSheet.create({
    container:{
        marginTop:Metrics.baseMargin,
        borderColor:'#E0E0E0',
        borderWidth:Metrics.ratio(0.95),
        borderRadius:Metrics.ratio(5),
        width:Metrics.screenWidth * 0.9,
        alignSelf:'center',
        paddingHorizontal:Metrics.baseMargin,
        // paddingTop:Metrics.smallMargin,
        flexDirection:'row',
        alignItems:'center'
    },
    image:{
        height: Metrics.ratio(25),
        width: Metrics.ratio(25),
        marginVertical:Metrics.doubleBaseMargin
      },
      rightContainer:{
        marginLeft: Metrics.smallMargin
      },
      title:{
        color: Colors.pink,
        fontWeight:'bold',
        fontSize:Metrics.ratio(14),
        marginTop:Metrics.smallMargin
      },
      inputStyle:{
        flex:1,
        minWidth:Metrics.screenWidth * 0.7
      }
})