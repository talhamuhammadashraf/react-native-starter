import React, { useState } from 'react';
import { View, TextInput, Image, Text } from 'react-native';
import { MailIcon } from '../../theme/Images';
import { Metrics } from '../../theme';
import styles from './styles';
export default ({
  keyboardType,
  title,
  placeholder,
  onChangeText,
  secureTextEntry,
  defaultValue,
  returnKeyType,
  containerStyles,
  inputStyles,
  onSubmitEditing,
  inputRef,
  imageStyle,
  ...inputProps
}) => {
  const [focused, toggleFocused] = useState(false);
  return (
    <View style={[styles.container,containerStyles]}>
      <Image source={MailIcon} resizeMode="contain" style={[styles.image,imageStyle]} />
      <View style={styles.rightContainer}>
        {focused && <Text style={styles.title}>{title}</Text>}
        <TextInput
          placeholder={focused ? placeholder : title}
          onFocus={() => toggleFocused(true)}
          onBlur={() => toggleFocused(false)}
          keyboardType={keyboardType}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          defaultValue={defaultValue}
          returnKeyType={returnKeyType || 'next'}
          style={[styles.inputStyle,inputStyles]}
          onSubmitEditing={onSubmitEditing}
          ref={inputRef}
          {...inputProps}
        />
      </View>
    </View>
  );
};
