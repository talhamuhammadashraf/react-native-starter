import React,{useEffect,useState} from 'react';
import { TouchableOpacity, Text,StyleSheet,Keyboard } from 'react-native';
import {Metrics,Colors} from '../../theme';
import { Ionicons } from '../../theme/Images';

export default ({ onPress, title ,style}) => {
  const [KeyboardOpen, setKeyboardState] = useState(false);
  useEffect(()=>{
    Keyboard.addListener("keyboardDidShow",()=>setKeyboardState(true))
    Keyboard.addListener("keyboardDidHide",()=>setKeyboardState(false))
  })
  console.log(KeyboardOpen)
  if(!KeyboardOpen){return(
  <TouchableOpacity onPress={onPress} style={[styles.container,style]}>
    <Text style={styles.title}>{title}</Text>
    <Ionicons name="ios-arrow-round-forward" size={Metrics.ratio(30)} color={Colors.white}/>
  </TouchableOpacity>
)}
return null
}


const styles  = StyleSheet.create({
    container:{
        backgroundColor:Colors.lightBlue,
        borderRadius:Metrics.ratio(7.5),
        alignSelf:'center',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingVertical:Metrics.baseMargin,
        width:Metrics.screenWidth*0.9,
        paddingHorizontal:Metrics.xDoubleBaseMargin
    },
    title:{ color: 'white',fontSize:Metrics.ratio(19),color:'white' }
})
