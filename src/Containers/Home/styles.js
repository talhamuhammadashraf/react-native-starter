import {StyleSheet} from 'react-native';
import { Metrics, Colors } from '../../theme';
import utils from '../../utils';
export default StyleSheet.create({
    elevation:{width:Metrics.screenWidth,
        height:utils.isPlatformAndroid()?0.6:Metrics.ratio(1.2),
        elevation:1.5,
        borderColor:Colors.border,
        backgroundColor:utils.isPlatformAndroid()?'transparent':Colors.border
        },
        headerContainer:{
            flexDirection: 'row',
            paddingHorizontal: Metrics.baseMargin,
            alignItems: 'center',
            justifyContent: 'space-between',
            width: Metrics.screenWidth,
            height: utils.isPlatformAndroid() ? 54 : 64,
          },
          headerTitle:{
            fontFamily: 'Montserrat-SemiBold',
            fontSize: Metrics.ratio(17)
          }
})