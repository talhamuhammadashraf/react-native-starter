import {
  createAppContainer,
  createSwitchNavigator
} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'
// import SplashScreen from 'react-native-splash-screen';
import {
  Home,
} from '../Containers';
import { Metrics } from '../theme';
import util from '../utils';
// SplashScreen.hide();

// SplashScreen.hide();

const DrawerStackNavigator = createStackNavigator(
  {
    home:Home,
  },
  // { headerMode: 'screen' },
);
// const DrawerStack = createDrawerNavigator(
//   {
//     home: DrawerStackNavigator,
//   },
//   {
//     drawerWidth: Metrics.screenWidth * 0.8,
//     contentComponent: DrawerComponent,
//   },
// );
// const AuthStack = createStackNavigator(
//   {
//     dashboard: Dashboard,
//     signin: SignIn,
//     signup: SignUp,
//   },
//   {
//     headerMode: 'screen'
//   }
// );
const ApplicationStack = createSwitchNavigator({
  // auth: AuthStack,
  drawerStack: DrawerStackNavigator
  // drawerStack: DrawerStack,
});
export default createAppContainer(ApplicationStack);
