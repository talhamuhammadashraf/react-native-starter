import Metrics from "./Metrics";

// const type = {
//   base: "Quicksand-Regular",
//   medium: "Quicksand-Medium",
//   bold: "Quicksand-Bold",
//   seventies: "Seventies"
// };

const type = {
  black:"Montserrat-Black",
  blackItalic:"Montserrat-BlackItalic",
  bold:"Montserrat-Bold",
  boldItalic:"Montserrat-BoldItalic",
  boldItalic:"Montserrat-BoldItalic",
  extraBoldItalic:"Montserrat-ExtraBoldItalic",
  extraBold:"Montserrat-ExtraBold",
  extraLight:"Montserrat-ExtraLight",
  extraLightItalic:"Montserrat-ExtraLightItalic",
  italic:"Montserrat-Italic",
  light:"Montserrat-Light",
  lightItalic:"Montserrat-LightItalic",
  mediumItalic:"Montserrat-MediumItalic",
  medium:"Montserrat-Medium",
  regular:"Montserrat-Regular",
  semiBold:"Montserrat-SemiBold",
  semiBoldItalic:"Montserrat-SemiBoldItalic",
  thin:"Montserrat-Thin",
  thinItalic:"Montserrat-ThinItalic",
  // base: "ProximaNova-Black",
  // // bold: "ProximaNova-Bold",
  // boldIt: "ProximaNova-BoldIt",
  // condLight: "ProximaNovaCond-Light",
  // condLightIt: "ProximaNovaCond-LightIt",
  // condLightWebfont: "proximanovacond-light-webfont",
  // // regular: "ProximaNovaCond-Regular",
  // regularit: "proximanovacond-regularit-webfont",
  // SemiboldIt: "ProximaNovaCond-SemiboldIt",
  // semibold: "proximanovacond-semibold-webfont",
  // Extrabold: "ProximaNova-Extrabold",
  // // light: "ProximaNova-Light",
  // lightItalic: "ProximaNova-LightItalic",
  // lightWebfont: "proximanova-light-webfont",
  // regularItalicWebfont: "proximanova-regularitalic-webfont",
  // regularWebfont: "proximanova-regular-webfont",
  // semiBoldItalic: "ProximaNova-SemiboldItalic",
  // semiBoldWebfont: "proximanova-semibold-webfont",
  // *****************  NEW Fonts
  
};

const size = {
  xxxSmall: Metrics.generatedFontSize(11),
  xxSmall: Metrics.generatedFontSize(13),
  xSmall: Metrics.generatedFontSize(14),
  small: Metrics.generatedFontSize(15),
  normal: Metrics.generatedFontSize(16),
  medium: Metrics.generatedFontSize(18),
  large: Metrics.generatedFontSize(20),
  xLarge: Metrics.generatedFontSize(22),
  xxLarge: Metrics.generatedFontSize(28),
  xxxLarge: Metrics.generatedFontSize(50)
};

export default {
  type,
  size
};
