// export const BASE_URL = "http://source.com/web/";   http://theriderspodadmin.com.au
// export const BASE_URL = 'http://192.168.5.117:4000'; //http://192.168.5.111:4000
export const BASE_URL = 'https://theriderspodadmin.com.au'; //http://192.168.5.117:4000
export const API_USER_NAME = '';
export const API_PASSWORD = '';
export const API_TIMEOUT = 80000;

export const API = '/api/';
export const VERSION = 'v1/';

export const LOGIN = `${BASE_URL}${API}${VERSION}login`;
export const API_GET_EVENT_TYPE = `${BASE_URL}${API}${VERSION}get-event-type`;
export const API_EDIT_EVENT = `${BASE_URL}${API}${VERSION}edit-event`;
export const API_CHANGE_PASSWORD = `${BASE_URL}${API}${VERSION}change-password`;
export const API_SIGNUP = `${BASE_URL}${API}${VERSION}sign-up`;
export const API_GET_IMAGE_PATH = `${BASE_URL}${API}${VERSION}get-image-path`;
export const API_ADD_EXERCISE_TO_CALENDAR = `${BASE_URL}${API}${VERSION}add-to-calendar`; //add-planner
export const API_ADD_PLANNER_EVENT = `${BASE_URL}${API}${VERSION}add-planner`;
export const API_PROFILE = `${BASE_URL}${API}${VERSION}get-profile`; //forgot-password
export const API_FORGOT_PASSWORD = `${BASE_URL}${API}${VERSION}forgot-password`;
export const API_UPDATE_PROFILE = `${BASE_URL}${API}${VERSION}update-profile`;
export const API_GET_FITNESS = `${BASE_URL}${API}${VERSION}get-fitness-category`; //add-exercise-to-calendar
export const API_ADD_EXERCISE = `${BASE_URL}${API}${VERSION}add-to-calendar`;
export const API_GET_FITNESS_ITEM = `${BASE_URL}${API}${VERSION}get-arms-workout-list`;
export const API_GET_FITNESS_ITEM_DETAIL = `${BASE_URL}${API}${VERSION}get-shoulder-workout-list`;
export const API_GET_PLANNER_LIST = `${BASE_URL}${API}${VERSION}get-planner-list`;
export const API_GET_NUTRITION = `${BASE_URL}${API}${VERSION}get-all-nutrition-list`;
export const API_GET_NUTRITION_DETAIL = `${BASE_URL}${API}${VERSION}get-nutrition-detail`; //get-ingredients-by-nutrition-name-id
export const API_GET_INGREDIENTS_BY_NUTRITION_NAME_ID = `${BASE_URL}${API}${VERSION}get-ingredients-by-nutrition-name-id`; //get-ingredient-steps
export const API_GET_INGREDIENTS_STEP = `${BASE_URL}${API}${VERSION}get-ingredient-steps`;
export const API_GET_NUTRITION_BY_ID = `${BASE_URL}${API}${VERSION}get-nutrition-by-id`;
export const API_GET_GOALS = `${BASE_URL}${API}${VERSION}get-goals`;
export const API_GET_GOALS_DETAIL = `${BASE_URL}${API}${VERSION}get-goals-detail`;
export const API_GET_RESULT = `${BASE_URL}${API}${VERSION}get-results`;
export const API_GET_SUBSCRIPTION = `${BASE_URL}${API}${VERSION}get-all-subscription`;
export const API_CANCEL_SUBSCRIPTION = `${BASE_URL}${API}${VERSION}cancel-subscription`;
// export const API_ADD_RESULT = `${BASE_URL}${API}${VERSION}add-result`;
export const API_GET_RESULT_DETAIL = `${BASE_URL}${API}${VERSION}get-result-detail`; //complete-goal
export const API_COMPLETE_GOAL = `${BASE_URL}${API}${VERSION}complete-goal`;
export const API_UPDATE_GOAL = `${BASE_URL}${API}${VERSION}update-goals`;
export const API_ADD_RESULT = `${BASE_URL}${API}${VERSION}add-result`;
export const API_ADD_GOAL = `${BASE_URL}${API}${VERSION}add-goal`;
export const API_CLEAR_NOTIFICATIONS = `${BASE_URL}${API}${VERSION}notifications/clear`;
export const API_NOTIFICATIONS = `${BASE_URL}${API}${VERSION}notifications`;
export const API_UPDATE_RESULT = `${BASE_URL}${API}${VERSION}update-result`;
export const API_REMOVE_RESULT = `${BASE_URL}${API}${VERSION}remove-result`; //remove-goal
export const API_REMOVE_GOAL = `${BASE_URL}${API}${VERSION}remove-goal`;
export const API_REMOVE_EVENT = `${BASE_URL}${API}${VERSION}remove-event`;
export const API_NOTIFICATION_STATE = `${BASE_URL}${API}${VERSION}update-notification-status`;
export const API_ADD_EVENT_TYPE = `${BASE_URL}${API}${VERSION}add-event-type`;
export const API_APPLY_DISCOUNT_CODE = `${BASE_URL}${API}${VERSION}apply-dicount-code`;

// export const API_GET_EVENT_TYPE = `${BASE_URL}${API}${VERSION}get-event-type`;

// API USER ROUTES
export const API_NEARBY_EVENT = `${API}api/sample`;

export const API_LOG = true;

export const ERROR_REQUEST_TIMEOUT = {
  error: 1,
  title: 'Request taking too much time',
  message:
    'We are sorry. It seems like something went wrong with your Internet connection',
};
export const ERROR_SERVER_CONNECTION = {
  error: 1,
  title: 'Connection Error',
  message: 'Server not available, bad dns.',
};
export const ERROR_401 = {
  error: 401,
  title: 'Invalid User Error',
  message: 'Unauthorized User',
};
export const ERROR_REQUEST_CANCEL = {
  error: 1,
  title: 'Request Canceled',
  message: 'You have canceled request.',
};
export const ERROR_NETWORK_NOT_AVAILABLE = {
  error: 1,
  title: 'Network not available',
  message: 'Please connect to the working Internet.',
};
export const ERROR_SOMETHING_WENT_WRONG = {
  error: 1,
  title: 'Whoops',
  message: 'Looks like something went wrong.',
};
export const ERROR_CLIENT = {
  error: 1,
  title: 'Whoops',
  message: 'Looks like we did something went wrong.',
};
